CREATE TABLE `baa`.`job_list` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(200) NOT NULL,
  `date_to_finish` DATE NOT NULL,
  `is_done` INT(1) NULL,
  `created` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`));
