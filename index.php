<?php

require 'vendor/autoload.php';

use \RedBeanPHP\Facade as R;

R::setup('mysql:host=localhost; dbname=baa', 'baa', 'baa');

$environment = getenv('APP_ENV');

// Bootstrap Slim application
$app = new \Slim\Slim(array(
    'env' => $environment,
));

require 'app/app.php';

$app->run();
R::close();