<?php
use \RedBeanPHP\Facade as R;

$app = \Slim\Slim::getInstance();
$post = $app->request->post();

if (empty($post)) { return; }

$post = json_decode($post, true);
$response['success'] = false;

try {
    foreach ($post as $item) {
        $bean = R::getRedBean()->dispense('job_list');

        $bean->text = $item['text'];
        $bean->date_to_finish = $item['date_to_finish'];
        $bean->is_done = $item['is_done'];

        R::store($bean);
    }
} catch (Exception $e) {
    $response['error'] = $e;

    echo json_encode($response);
}

$response['success'] = true;

echo json_encode($response);
