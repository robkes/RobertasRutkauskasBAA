<?php
use \RedBeanPHP\Facade as R;

$jobs = R::findAndExport('job_list');

foreach ($jobs as $job) {
    echo json_encode($job);
}
