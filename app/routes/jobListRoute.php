<?php

$app->get('/', function() use ($app) {
    include 'app/actions/jobList-get.php';
});

$app->post('/', function() use ($app) {
    include 'app/actions/jobList-post.php';
});

